package org.evacuate.gateway;

import eu.evacuate.og.ws.client.post.CreateOperations;
import eu.evacuate.og.ws.dto.ControlDTO;
import eu.evacuate.og.ws.dto.ResponseDTO;
import eu.evacuate.og.ws.dto.TetraDTO;
import org.evacuate.dto.TetraOG;
import org.evacuate.dto.filesdto;
import org.evacuate.filemanager.filewriter;
import org.evacuate.filemanager.listfiles;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.nio.file.Files;

import static org.springframework.web.bind.annotation.RequestMethod.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by tdim on 12/10/2015.
 */
@RestController
@RequestMapping("/tetrarouter")
public class tetrarouter {

    public String GetStatus() {
        return "Router Online";
    }

    @RequestMapping(value = "/status", method = GET, produces = APPLICATION_JSON_VALUE)
    public String hello() {
        return GetStatus();

    }

    @RequestMapping(value = "/routetoog", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public TetraOG update(@RequestBody TetraOG params) {

        System.out.print("Tetra Message Received : "+"Message : "+params.getMessage());

        filewriter fw=new filewriter();

        fw.write(1,params.getMessage());
        params.setStatus("OK");

        CreateOperations pConnector= new CreateOperations();
        ControlDTO pControl=new ControlDTO();
        pControl.setSystems("OG");

        try {
            pConnector.evacControlSystem(pControl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseDTO pReturn=new ResponseDTO();

        TetraDTO pTetra= new TetraDTO();
        pTetra.setProcedure("3786");
        pTetra.setMessage(params.getMessage());
        try {
            pReturn=pConnector.evacTetra(pTetra);
            System.out.print(pReturn.getTaskId());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return params;
    }

    @RequestMapping(value = "/examplejson", method = GET, produces = APPLICATION_JSON_VALUE)
    public TetraOG examplejson() {
        TetraOG example = new TetraOG();
        example.setStatus("Return Status to be populated by the call");
        example.setId("Set this to imei-datetime");
        example.setMessage("Your message to be send by tetra");
        return example;

    }

    public String readfile(String filename)
    {
        try{
            // Open the file that is the first
            // command line parameter
            FileInputStream fstream = new FileInputStream(filename);
            // Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            //Read File Line By Line
            while ((strLine = br.readLine()) != null)   {
                // Print the content on the console
                System.out.println (strLine);
                return strLine;
            }
            //Close the input stream
            in.close();
        }catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
            return "";
        }
return "";
    }

    @RequestMapping(value = "/gethistory", method = GET, produces = APPLICATION_JSON_VALUE)
    public filesdto gethistory() {
        filesdto pfiles=new filesdto();
        listfiles pfilesd= new listfiles();
        String ss="";
        String ss1="";
        File[] preturn= pfilesd.finder("/opt/evacuate/tetra/");
        for(int i=0;i<preturn.length;i++)
        {
            ss=ss+","+preturn[i].getName();
            ss1=ss1+","+readfile(preturn[i].getAbsolutePath());

        }

        return pfiles;

    }


}
