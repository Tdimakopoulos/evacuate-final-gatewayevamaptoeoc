package org.evacuate.gateway;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.bind.annotation.RequestMethod.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by tdim on 8/12/2015.
 */
@RestController
@RequestMapping("/api")
public class TestService {

    @RequestMapping(value = "/alert", method = GET, produces = APPLICATION_JSON_VALUE)
    public String hello() {
        return "Hello";

    }
}
