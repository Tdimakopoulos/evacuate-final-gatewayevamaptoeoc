package org.evacuate.gateway;

import org.evacuate.dto.AlertBO;
import org.evacuate.dto.AlertFR;
import org.evacuate.dto.AlertVisitor;
import org.evacuate.dto.TetraOG;
import org.evacuate.filemanager.filewriter;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by tdim on 12/10/2015.
 */
@RestController
@RequestMapping("/alertrouter")
public class alertrouter {

    public String GetStatus() {
        return "Router Online";
    }

    @RequestMapping(value = "/status", method = GET, produces = APPLICATION_JSON_VALUE)
    public String status() {
        return GetStatus();

    }

    @RequestMapping(value = "/frombo", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public AlertBO frombo(@RequestBody AlertBO params) {


        System.out.print("Alertm Receive from BO(Initializing Routing) Details : "+"Alert : "+params.getMessage());


        filewriter fw=new filewriter();

        fw.write(2,"Backoffice alert : "+params.getMessage());

        params.setStatus("OK");
        return params;
    }

    @RequestMapping(value = "/fromfr", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public AlertFR fromfr(@RequestBody AlertFR params) {

        System.out.print("Alertm Receive from FR(Initializing Routing) Details : "+"FR alert : "+params.getMessage()+",FR_ID:"+params.getFr_id()+",FR_NAME:"+params.getFr_name()+",FR_SURNAME:"+params.getFr_surname());
        filewriter fw=new filewriter();

        fw.write(2,"FR alert : "+params.getMessage()+",FR_ID:"+params.getFr_id()+",FR_NAME:"+params.getFr_name()+",FR_SURNAME:"+params.getFr_surname());

        params.setStatus("OK");
        return params;
    }

    @RequestMapping(value = "/fromvisitor", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public AlertVisitor fromvisitor(@RequestBody AlertVisitor params) {

        System.out.print("Alertm Receive from Visitor(Initializing Routing) Details : "+"Alert : "+params.getMessage());

        filewriter fw=new filewriter();

        fw.write(2,"Visitor alert : "+params.getMessage());

        params.setStatus("OK");
        return params;
    }


    @RequestMapping(value = "/examplejsonfr", method = GET, produces = APPLICATION_JSON_VALUE)
    public AlertFR examplejsonfr() {
        AlertFR example = new AlertFR();
        example.setMessage("Message for the alert");
        example.setStatus("The return status");
        example.setLevels("Alert level");
        example.setSender_uuid("Sender ID");
        example.setSeverity("Alert Severity");
        example.setUuid(" Alert UUID");
        return example;

    }


    @RequestMapping(value = "/examplejsonvisitor", method = GET, produces = APPLICATION_JSON_VALUE)
    public AlertVisitor examplejsonvis() {
        AlertVisitor example = new AlertVisitor();
        example.setMessage("Message for the alert");
        example.setStatus("The return status");
        example.setLevels("Alert level");
        example.setSender_uuid("Sender ID");
        example.setSeverity("Alert Severity");
        example.setUuid(" Alert UUID");
        return example;

    }


    @RequestMapping(value = "/examplejsonbo", method = GET, produces = APPLICATION_JSON_VALUE)
    public AlertBO examplejsonbo() {
        AlertBO example = new AlertBO();
        example.setMessage("Message for the alert");
        example.setStatus("The return status");
        example.setType("Type of the alert");
        example.setSeverity("Alert Severity");
        example.setUuid(" Alert UUID");
        return example;

    }
}
