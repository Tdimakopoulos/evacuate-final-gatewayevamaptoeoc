package org.evacuate.filemanager;

import java.io.*;
import java.util.Date;

/**
 * Created by tdim on 12/13/2015.
 */
public class filewriter {

    String tetra="/opt/evacuate/tetra/";
    String alert="/opt/evacuate/alert/";

    public void write(int type,String text)
    {
        String szfilename=(new Date()).toString()+".csv";
        String cpath="";
        if (type==1)
        {
            cpath=tetra+szfilename;
        }
        if (type==2)
        {
            cpath=alert+szfilename;
        }
        try{
            // Create file
            FileWriter fstream = new FileWriter(cpath);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(text);
            //Close the output stream
            out.close();
        }catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }
}


