package org.evacuate.filemanager;

import java.io.File;
import java.io.FilenameFilter;


/**
 * Created by tdim on 12/15/2015.
 */
public class listfiles {
    public File[] finder( String dirName){
        File dir = new File(dirName);

        return dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename)
            { return filename.endsWith(".csv"); }
        } );

    }
}


