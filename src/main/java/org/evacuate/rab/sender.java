package org.evacuate.rab;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * Created by tdim on 12/10/2015.
 */
public class sender {

    String EXCHANGE_NAME = "topic_logs";

    //kern.critical
    //test message string
    public void senderrq(String routingKey, String message)
            throws Exception {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("193.92.114.116");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "topic");

        channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes());
//        System.out.println(" [x] Sent '" + routingKey + "':'" + message + "'");

        connection.close();
    }


}
