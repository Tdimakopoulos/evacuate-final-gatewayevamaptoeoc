package org.evacuate.rab;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * Created by tdim on 12/10/2015.
 */

public class receiver {

    String EXCHANGE_NAME = "topic_logs";

    //kern.critical
    public void Receivermain(String bindingKey) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("193.92.114.116");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String queueName = channel.queueDeclare().getQueue();


        channel.queueBind(queueName, EXCHANGE_NAME, bindingKey);


        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + envelope.getRoutingKey() + "':'" + message + "'");
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }


}
