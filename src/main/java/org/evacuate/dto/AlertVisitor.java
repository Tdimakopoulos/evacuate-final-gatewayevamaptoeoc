package org.evacuate.dto;

/**
 * Created by tdim on 12/10/2015.
 */
public class AlertVisitor {

    private String message;
    private String uuid;
    private String severity;
    private String levels;
    private String sender_uuid;
    private String status;

    private String message_type;
    private String px_id;
    private String px_name;
    private String px_surname;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getLevels() {
        return levels;
    }

    public void setLevels(String levels) {
        this.levels = levels;
    }

    public String getSender_uuid() {
        return sender_uuid;
    }

    public void setSender_uuid(String sender_uuid) {
        this.sender_uuid = sender_uuid;
    }

    public String getMessage_type() {
        return message_type;
    }

    public void setMessage_type(String message_type) {
        this.message_type = message_type;
    }

    public String getPx_id() {
        return px_id;
    }

    public void setPx_id(String px_id) {
        this.px_id = px_id;
    }

    public String getPx_name() {
        return px_name;
    }

    public void setPx_name(String px_name) {
        this.px_name = px_name;
    }

    public String getPx_surname() {
        return px_surname;
    }

    public void setPx_surname(String px_surname) {
        this.px_surname = px_surname;
    }
}
