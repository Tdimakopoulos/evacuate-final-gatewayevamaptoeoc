package org.evacuate.dto;

/**
 * Created by tdim on 12/10/2015.
 */
public class AlertFR {

    private String message;
    private String uuid;
    private String severity;
    private String levels;
    private String sender_uuid;
    private String status;

    private String message_type;
    private String fr_id;
    private String fr_name;
    private String fr_surname;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getLevels() {
        return levels;
    }

    public void setLevels(String levels) {
        this.levels = levels;
    }

    public String getSender_uuid() {
        return sender_uuid;
    }

    public void setSender_uuid(String sender_uuid) {
        this.sender_uuid = sender_uuid;
    }

    public String getMessage_type() {
        return message_type;
    }

    public void setMessage_type(String message_type) {
        this.message_type = message_type;
    }

    public String getFr_id() {
        return fr_id;
    }

    public void setFr_id(String fr_id) {
        this.fr_id = fr_id;
    }

    public String getFr_name() {
        return fr_name;
    }

    public void setFr_name(String fr_name) {
        this.fr_name = fr_name;
    }

    public String getFr_surname() {
        return fr_surname;
    }

    public void setFr_surname(String fr_surname) {
        this.fr_surname = fr_surname;
    }
}
