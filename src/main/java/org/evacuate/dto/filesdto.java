package org.evacuate.dto;

/**
 * Created by tdim on 12/15/2015.
 */
public class filesdto {
    private String filename;
    private String message;


    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
