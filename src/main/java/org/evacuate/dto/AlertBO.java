package org.evacuate.dto;

/**
 * Created by tdim on 12/10/2015.
 */
public class AlertBO {

    private String message;
    private String uuid;
    private String severity;
    private String type;
    private String status;

    private String message_type;
    private String bo_id;
    private String bo_name;
    private String bo_surname;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage_type() {
        return message_type;
    }

    public void setMessage_type(String message_type) {
        this.message_type = message_type;
    }

    public String getBo_id() {
        return bo_id;
    }

    public void setBo_id(String bo_id) {
        this.bo_id = bo_id;
    }

    public String getBo_name() {
        return bo_name;
    }

    public void setBo_name(String bo_name) {
        this.bo_name = bo_name;
    }

    public String getBo_surname() {
        return bo_surname;
    }

    public void setBo_surname(String bo_surname) {
        this.bo_surname = bo_surname;
    }
}
