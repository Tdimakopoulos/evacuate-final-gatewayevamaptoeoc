package org.evacuate.org.evacuate.client.evamapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by tdim on 1/7/2016.
 */
public class restclient {
    public  void SendAlertToEva(String message,String uuid,String severity) throws ClientProtocolException, IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://193.92.114.115/api/v1/cop_messages");

        JSONObject json = new JSONObject();
        try {
            json.put("message", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            json.put("uuid", uuid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            json.put("severity", severity);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity input = new StringEntity(json.toString());
        input.setContentType("application/json");

        post.setEntity(input);
        HttpResponse response = client.execute(post);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line = "";
        while ((line = rd.readLine()) != null) {
            System.out.println(line);
        }
    }

    public  void SendTetraToEva(String message,String uuid,String severity) throws ClientProtocolException, IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://193.92.114.115/api/v1/tetra_messages");

        JSONObject json = new JSONObject();
        try {
            json.put("message", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            json.put("uuid", uuid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            json.put("severity", severity);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity input = new StringEntity(json.toString());
        input.setContentType("application/json");

        post.setEntity(input);
        HttpResponse response = client.execute(post);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line = "";
        while ((line = rd.readLine()) != null) {
            System.out.println(line);
        }
    }
}

