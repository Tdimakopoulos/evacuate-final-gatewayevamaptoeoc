/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.og.ws.dto;

/**
 *
 * @author tdim
 */
public class DigitalSignDTO {

    private String procedure;
    private String play; //PLAY,STOP
    private String filename;

    /**
     * @return the procedure
     */
    public String getProcedure() {
        return procedure;
    }

    /**
     * @param procedure the procedure to set
     */
    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    /**
     * @return the play
     */
    public String getPlay() {
        return play;
    }

    /**
     * @param play the play to set
     */
    public void setPlay(String play) {
        this.play = play;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }
}
